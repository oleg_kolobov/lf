/**
 * index.js -- API server
 */
require('dotenv').config();

const app = require('./server');

app.set('port', process.env.PORT || 3000);

const server = app.listen(app.get('port'), function() {
    console.log('Server at', server.address().port)
});

module.exports = app;
