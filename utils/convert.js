#!/usr/bin/env node
var readline = require('readline');
//var crypto = require('crypto');
//var algorithm = 'sha1';
//var salt = 'pechenki';

var rl = readline.createInterface({
    input: process.stdin,
});

console.log('exports.data=[');

rl.on('line', function(row) {
    var col = row.split(/[ \t,]+/);
    if (col.length>1) {
        var userid = col[0];
        var docid = col[1];

        //var hmac = crypto.createHmac(algorithm, salt);
        //hmac.update(lcn);
        //var docid = hmac.digest('hex');

        console.log('{userid:"'+ userid + '",docid:"' + docid.replace(/\\/g, '\\\\') + '"},');
    }
});

rl.on('close', function() {
    console.log('];');
});
