'use strict'

const bookshelf = require('../bookshelf');

const Filter = require('../server/filters/model')

const specs = [
    { filter_name: 'cfk4', filter_parameters: '{"k":4}', dataset: 'test'},
    { filter_name: 'cfk5', filter_parameters: '{"k":5}', dataset: 'test'},
    { filter_name: 'cfk10', filter_parameters: '{"k":10}', dataset: 'test'}
];


var results = []
specs.forEach(function(spec) {
    let model = {
        name: spec.filter_name,
        parameters: spec.filter_parameters,
        matrices: [
            { dataset: spec.dataset }
        ]
    };
    results.push(Filter.forge(model).save().then(function(filter) {
        console.log(filter.toJSON());
    }));
});

Promise.all(results).then(function() {
    bookshelf.knex.destroy();
    console.log("Done.");
});



