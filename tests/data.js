require('dotenv').config();
const Chance = require('chance');
const chai = require('chai');
const should = chai.should();
const expect = chai.expect;
const chaiHttp = require('chai-http');
const server = require('../server');

chai.use(chaiHttp);

const agent = chai.request.agent(server);
const verbose = (process.env.VERBOSE === '1') ? true : false;
const chance = new Chance();

describe('Data', function() {

    let _models = [];

    describe('POST /api/v1/data', function() {
        let model = {
            dataset: chance.string(),
            userid: chance.string(),
            docid: chance.string()
        };

        it('it should be create a data', function(done) {
            agent.post('/api/v1/data').send(model).end(function(err, res) {
                if (verbose) {
                    console.log(model);
                    console.log(res.body);
                }
                res.should.have.status(201);
                if (verbose) console.log(res.body);
                res.body.should.be.a('object');
                done();
            });
        });

    });

    describe('GET /api/v1/count/count', function() {
        it('it should be count data', function(done) {
            agent.get('/api/v1/data/count').end(function(err, res) {
                res.should.have.status(200);
                if (verbose) console.log(res.body);
                res.body.should.be.a('object');
                done();
            });
        });

    });

    describe('GET /api/v1/data', function() {
        it('it should be list of data', function(done) {
            agent.get('/api/v1/data').query({offset: 0, limit: 10}).end(function(err, res) {
                res.should.have.status(200);
                if (verbose) console.log(res.body);
                res.body.should.be.a('object');
                _models = res.body.data;
                done();
            });
        });
    });

    describe('GET /api/v1/data/:id', function() {
        it('it should be get data by id', function(done) {
            let last = _models.length - 1;
            agent.get('/api/v1/data/' + _models[last].id).end(function(err, res) {
                res.should.have.status(200);
                if (verbose) console.log(res.body);
                res.body.should.be.a('object');
                done();
            });
        });
    });

    describe('PUT /api/v1/data/:id', function() {

        it('it should be update a field of data', function(done) {
            let last = _models.length - 1;

            _models[last].dataset = chance.string();

            agent.put('/api/v1/data/' + _models[last].id)
                .send(_models[last])
                .end(function(err, res) {
                    res.should.have.status(200);
                    if (verbose) console.log(res.body);
                    res.body.should.be.a('object');
                    done();
                });
        });

        it('it should be get data by id (with updated field)', function(done) {
            let last = _models.length - 1;
            agent.get('/api/v1/data/' + _models[last].id).end(function(err, res) {
                res.should.have.status(200);
                if (verbose) console.log(res.body);
                res.body.should.be.a('object');
                done();
            });
        });

    });

    describe('DELETE /api/v1/data/:id', function() {
        it('it should be delete data by id', function(done) {
            let last = _models.length - 1;
            agent.delete('/api/v1/data/' + _models[last].id).end(function(err, res) {
                res.should.have.status(200);
                if (verbose) console.log(res.body);
                res.body.should.be.a('object');
                done();
            });
        });
    });

});
