require('dotenv').config();
const Chance = require('chance');
const chai = require('chai');
const should = chai.should();
const expect = chai.expect;
const chaiHttp = require('chai-http');
const server = require('../server');

chai.use(chaiHttp);

const agent = chai.request.agent(server);
const verbose = (process.env.VERBOSE === '1') ? true : false;
const chance = new Chance();

describe('Recommendation', function() {

    let _models = [];

    describe('POST /api/v1/recommendations', function() {
        let model = {
            docid: chance.natural()
        };

        it('it should be create a recommendation', function(done) {
            agent.post('/api/v1/recommendations').send(model).end(function(err, res) {
                if (verbose) {
                    console.log(model);
                }
                res.should.have.status(201);
                if (verbose) console.log(res.body);
                res.body.should.be.a('object');
                done();
            });
        });

    });

    describe('GET /api/v1/recommendations/count', function() {
        it('it should be count recommendation', function(done) {
            agent.get('/api/v1/recommendations/count').end(function(err, res) {
                res.should.have.status(200);
                if (verbose) console.log(res.body);
                res.body.should.be.a('object');
                done();
            });
        });

    });

    describe('GET /api/v1/recommendations', function() {
        it('it should be list of recommendation', function(done) {
            agent.get('/api/v1/recommendations').query({offset: 0, limit: 10}).end(function(err, res) {
                res.should.have.status(200);
                if (verbose) console.log(res.body);
                res.body.should.be.a('object');
                _models = res.body.data;
                done();
            });
        });
    });

    describe('GET /api/v1/recommendations/:id', function() {
        it('it should be get recommendation by id', function(done) {
            let last = _models.length - 1;
            agent.get('/api/v1/recommendations/' + _models[last].id).end(function(err, res) {
                res.should.have.status(200);
                if (verbose) console.log(res.body);
                res.body.should.be.a('object');
                done();
            });
        });
    });

    describe('PUT /api/v1/recommendations/:id', function() {

        it('it should be update a field of recommendation', function(done) {
            let last = _models.length - 1;

            _models[last].docid = chance.natural();

            agent.put('/api/v1/recommendations/' + _models[last].id)
                .send(_models[last])
                .end(function(err, res) {
                    res.should.have.status(200);
                    if (verbose) console.log(res.body);
                    res.body.should.be.a('object');
                    done();
                });
        });

        it('it should be get recommendation by id (with updated field)', function(done) {
            let last = _models.length - 1;
            agent.get('/api/v1/recommendations/' + _models[last].id).end(function(err, res) {
                res.should.have.status(200);
                if (verbose) console.log(res.body);
                res.body.should.be.a('object');
                done();
            });
        });

    });

    describe('DELETE /api/v1/recommendations/:id', function() {
        it('it should be delete recommendation by id', function(done) {
            let last = _models.length - 1;
            agent.delete('/api/v1/recommendations/' + _models[last].id).end(function(err, res) {
                res.should.have.status(200);
                if (verbose) console.log(res.body);
                res.body.should.be.a('object');
                done();
            });
        });
    });

});
