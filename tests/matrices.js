require('dotenv').config();
const Chance = require('chance');
const chai = require('chai');
const should = chai.should();
const expect = chai.expect;
const chaiHttp = require('chai-http');
const server = require('../server');

chai.use(chaiHttp);

const agent = chai.request.agent(server);
const verbose = (process.env.VERBOSE === '1') ? true : false;
const chance = new Chance();

describe('Matrix', function() {

    let _models = [];

    describe('POST /api/v1/matrices', function() {
        let model = {
            complete: false
        };

        it('it should be create a matrix', function(done) {
            agent.post('/api/v1/matrices').send(model).end(function(err, res) {
                if (verbose) {
                    console.log(model);
                }
                res.should.have.status(201);
                if (verbose) console.log(res.body);
                res.body.should.be.a('object');
                done();
            });
        });

    });

    describe('GET /api/v1/count/count', function() {
        it('it should be count matrix', function(done) {
            agent.get('/api/v1/matrices/count').end(function(err, res) {
                res.should.have.status(200);
                if (verbose) console.log(res.body);
                res.body.should.be.a('object');
                done();
            });
        });

    });

    describe('GET /api/v1/matrices', function() {
        it('it should be list of matrix', function(done) {
            agent.get('/api/v1/matrices').query({offset: 0, limit: 10}).end(function(err, res) {
                res.should.have.status(200);
                if (verbose) console.log(res.body);
                res.body.should.be.a('object');
                _models = res.body.data;
                done();
            });
        });
    });

    describe('GET /api/v1/matrices/:id', function() {
        it('it should be get matrix by id', function(done) {
            let last = _models.length - 1;
            agent.get('/api/v1/matrices/' + _models[last].id).end(function(err, res) {
                res.should.have.status(200);
                if (verbose) console.log(res.body);
                res.body.should.be.a('object');
                done();
            });
        });
    });

    describe('PUT /api/v1/matrices/:id', function() {

        it('it should be update a field of matrix', function(done) {
            let last = _models.length - 1;

            _models[last].complete = true;

            agent.put('/api/v1/matrices/' + _models[last].id)
                .send(_models[last])
                .end(function(err, res) {
                    res.should.have.status(200);
                    if (verbose) console.log(res.body);
                    res.body.should.be.a('object');
                    done();
                });
        });

        it('it should be get matrix by id (with updated field)', function(done) {
            let last = _models.length - 1;
            agent.get('/api/v1/matrices/' + _models[last].id).end(function(err, res) {
                res.should.have.status(200);
                if (verbose) console.log(res.body);
                res.body.should.be.a('object');
                done();
            });
        });

    });

    /*
    describe('POST /api/v1/matrices/:id/:command', function() {

        it('it should be invoke the create command of matrix', function(done) {
            let last = _models.length - 1;
            agent.post('/api/v1/matrices/' + _models[last].id + '/create')
                .end(function(err, res) {
                    if (verbose) console.log(res.body);
                    res.should.have.status(200);
                    //if (verbose) console.log(res.body);
                    res.body.should.be.a('object');
                    done();
                });
        });

        it('it should be invoke the drop command of matrix', function(done) {
            let last = _models.length - 1;
            agent.post('/api/v1/matrices/' + _models[last].id + '/drop')
                .end(function(err, res) {
                    if (verbose) console.log(res.body);
                    res.should.have.status(200);
                    //if (verbose) console.log(res.body);
                    res.body.should.be.a('object');
                    done();
                });
        });
        it('it should be invoke the bad command of matrix', function(done) {
            let last = _models.length - 1;
            agent.post('/api/v1/matrices/' + _models[last].id + '/bad')
                .end(function(err, res) {
                    if (verbose) console.log(res.body);
                    res.should.have.status(500);
                    //if (verbose) console.log(res.body);
                    res.body.should.be.a('object');
                    done();
                });
        });
    });
    */

    describe('DELETE /api/v1/matrices/:id', function() {
        it('it should be delete matrix by id', function(done) {
            let last = _models.length - 1;
            agent.delete('/api/v1/matrices/' + _models[last].id).end(function(err, res) {
                res.should.have.status(200);
                if (verbose) console.log(res.body);
                res.body.should.be.a('object');
                done();
            });
        });
    });

});
