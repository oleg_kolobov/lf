const rows = require('./dataset/acirctest.js').data;
exports.seed = function(knex) {
    return knex('data').del().then(function() {
        return knex.batchInsert('data', rows);
    });
}
