const rows = require('./dataset/acirc2015.js').data;
exports.seed = function(knex) {
    return knex('data').del().then(function() {
        return knex.batchInsert('data', rows);
    });
}
