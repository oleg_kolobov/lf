
exports.up = function(knex) {
    return knex.schema.createTable('data', function(table) {
        table.increments('id').unsigned().primary();
        table.string('dataset').notNull().defaultTo('test');
        table.string('userid').notNull();
        table.string('docid').notNull();
    });
};

exports.down = function(knex) {
    return knex.schema.dropTable('data');
};
