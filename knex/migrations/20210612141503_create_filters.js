
exports.up = function(knex) {
    return knex.schema.createTable('filters', function(table) {
        table.increments('id').unsigned().primary();
        table.string('name').notNull();
        table.string('parameters');
    });
};

exports.down = function(knex) {
    return knex.schema.dropTable('filters');
};
