
exports.up = function(knex) {
    return knex.schema.createTable('lrs_msim', function(table) {
        table.increments('id').unsigned().primary();
        table.string('docid').notNull();
        table.integer('count');
        table.jsonb('sim');
        table.jsonb('doc');
        table.timestamp('ctime', {useTz: true}).defaultTo(knex.fn.now());
    });
};

exports.down = function(knex) {
    return knex.schema.dropTable('lrs_msim');
};
