
exports.up = function(knex) {
    return knex.schema.createTable('recommendations', function(table) {
        table.increments('id').unsigned().primary();
        table.string('docid').notNull();
        table.integer('count');
        table.jsonb('sim');
        table.jsonb('doc');
        table.integer('matrix_id').references('matrices.id');
    });
};

exports.down = function(knex) {
    return knex.schema.dropTable('recommendations');
};
