
exports.up = function(knex) {
    return knex.schema.createTable('lrs', function(table) {
        table.increments('id').unsigned().primary();
        table.string('userid').notNull();
        table.string('docid').notNull();
        table.timestamp('ctime', {useTz: true}).defaultTo(knex.fn.now());
    });
};

exports.down = function(knex) {
    return knex.schema.dropTable('lrs');
};
