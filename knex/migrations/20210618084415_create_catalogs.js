
exports.up = function(knex) {
    return knex.schema.createTable('catalogs', function(table) {
        table.increments('id').unsigned().primary();
        table.string('name').notNull();
        table.string('url').notNull();
    });
};

exports.down = function(knex) {
    return knex.schema.dropTable('catalogs');
};
