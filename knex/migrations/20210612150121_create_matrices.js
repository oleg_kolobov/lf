
exports.up = function(knex) {
    return knex.schema.createTable('matrices', function(table) {
        table.increments('id').unsigned().primary();
        table.string('dataset').notNull().defaultTo('test');
        table.boolean('complete').notNullable().defaultTo(false);
        table.integer('filter_id').references('filters.id');
    });
};

exports.down = function(knex) {
    return knex.schema.dropTable('matrices');
};
