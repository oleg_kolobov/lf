const knex = require('../knex/knex');
const bookshelf = require('bookshelf')(knex);
//const virtuals = require('bookshelf-virtuals-plugin');
const relations = require('bookshelf-relations');

//bookshelf.plugin(virtuals);
bookshelf.plugin(relations);

module.exports = bookshelf;
