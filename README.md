# README

lf -- рекомендательный сервис для электронного каталога библиотеки.

# Подготовка к работе

*Подразумевается, что программа будет разворачиваться в операционной системе FreeBSD версии 11.3
или более поздних версий.*

* Установить пакеты программ
* Создать конфигурацию PostgreSQL
* Создать конфигурацию gearmand

## Установка пакетов программ

```sh

#pkg install git
#pkg install node 
#pkg install www/npm
#pkg install www/yarn
#pkg install postgresql93-server
#pkg install gearmand
#npm install -g knex
```

## Создание кофигурации PostgreSQL

* Инициализировать PostgreSQL и настроить автоматический запуск и останов
* Создать пользователя `lfusr1`, создать базу `lf`
* Разрешить удаленный доступ с определенного IP адреса.

```sh
#echo 'postgresql_enable="YES"' >> /etc/rc.conf
#servive postgresql initdb
#service postgresql start 
```

*Примечание: при следующей перезагрузке сервер PostgreSQL будет автоматически запускаться*

***Разрешить доступ к PostgeSQL***

В файле `/usr/local/pgsql/data/pg_hba.conf` указать IP адрес клиента

```sh
...
host    all             all             x.x.x.x/32            trust
...
```

, где x.x.x.x -- это IP клиента, для которого открывается доступ к PostgreSQL.

```sh
$ sudo vi /usr/local/pgsql/data/pg_hba/conf
...редактирование, сохранение изменений
```

***Рестарт PostgreSQL***

```sh
#service postgresql restart
```

После рестарта сервер PostgreSQL готов для работы.


***Создание базы данных***

Создать пользователя `lfusr1`:

```sh
#su -u pgsql createuser -SdrP lfusr1
...ввести пароль
```

Создать базу данных `lf`:

```sh
# su -u pgsql createdb lf --owner=lfusr1
```

## Создание конфигурации gearmand

```sh
#echo 'gearmand_enable="YES"' >> /etc/rc.conf
#servive gearmand start
```

# Развертывание приложения

Для развертывения приложения необходимо извлечь исзодный код и установить зависимые пакеты Node.js:

```sh
$git clone https://oleg_kolobov@bitbucket.org/oleg_kolobov/lf.git
$cd lf
$yarn install
```

# Тестирование

```sh
$yarn test
```
