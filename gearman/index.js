/* jshint esversion: 6 */
const Gearman = require('node-gearman');
module.exports = new Gearman(process.env.GEARMAN_HOST, process.env.GEARMAN_PORT);

