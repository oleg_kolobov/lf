#!/bin/sh
#set -x
PROG1="node ./prog1.js"
PROG2="node ./prog2.js"
for k in 1 2 3 4 5
do
	${PROG1} $k
	for N in 1 2 3 4 5 6
	do
		${PROG2} $N > out_k${k}_N${N}.csv
	done
done
