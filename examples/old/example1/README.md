1. Example for SHA1

```
var crypto = require('crypto');

function sha1( data ) {
         var generator = crypto.createHash('sha1');
              generator.update( data )  
                   return generator.digest('hex') 
}

console.log( sha1('adrian') )
```
