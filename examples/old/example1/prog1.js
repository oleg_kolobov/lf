// prog1.js
// @k  number

var usage = function() {
    console.log('usage: node prog1.js k');
}

var k;

if (process.argv.length>2) {
    k = process.argv[2];
    console.log('k=' + k);
} else {
    usage();
}
