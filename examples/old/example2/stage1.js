#!/usr/bin/env node
var crypto = require('crypto');
var salt = 'pechenki';
var readline = require('readline');

var rl = readline.createInterface({
    input: process.stdin,
});

console.log('exports.data=[');

rl.on('line', function(row) {
    var col = row.split(/[ \t,]+/);
    if (col.length>1) {
        console.log('{userid:"', col[0], '",docid:"',col[1].replace(/\\/g, '\\\\'),'"},');
    }
});

rl.on('close', function() {
    console.log('];');
});


