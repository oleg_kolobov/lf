// database.js
var pg = require('pg');
var dburl = process.env.DATABASE_URL || 'postgres://lfusr1:library@localhost/lf';
var client = new pg.Client(dburl);

client.connect(function(err) {
    if (err) {
        console.error(err);
    }
    var query = client.query("EXPLAIN ANALYZE SELECT * FROM lrs;");
    var results = [];
    query.on('row', function(row) {
        results.push(row);
    });
    query.on('end', function() {
        client.end();
    });
});


