/**
 * stage2.js - load data from a file
 */

const rows = require('./tmp/_data1.js').data;
const chunk_size = 1000;
const knex = require('./knex/knex.js');

knex.batchInsert('lrs', rows, chunk_size).returning('id').then(function(ids) {
    console.log(ids);
}).catch(function(err) {
    console.log(err.message || err);
}).finally(function() {
    knex.destroy();
});
