/**
 * stage5.js - show table lrs_msim
 */
const _ = require('underscore');
const knex = require('./knex/knex.js');

knex('recommendations').select().then(function(rows) {
    _.each(rows, function(row) { console.log(row); });
}).finally(function() {
    knex.destroy();
});
