/**
 * statge4.js - create or update matrix of similiraty
 */
const knex = require('./knex/knex.js');
var _ = require('underscore');
const chunk_size = 1000;
function add(d1, d2, sim) {
    var k = 5;
    var i;
    if (!d1.sim) {
        d1.sim = [];
        d1.doc = [];
        for (i=0; i<k; ++i) {
            d1.sim[i] = d1.sim[i] || 0;
            d1.doc[i] = d1.doc[i] || 0;
        }
    }

    for (i=0; i<k; ++i) {
        if (d1.sim[i] < sim) {
            var keep_sim = d1.sim[i];
            var keep_doc = d1.doc[i];
            d1.sim[i] = sim;
            d1.doc[i] = d2.docid;
            for(j=i+1; j<k; ++j) {
                var keep2_sim = d1.sim[j];
                var keep2_doc = d1.doc[j];
                d1.sim[j] = keep_sim;
                d1.doc[j] = keep_doc;
                keep_sim = keep2_sim;
                keep_doc = keep2_doc;
            }
            break;
        }
    }
}

knex('lrs').select(['docid', knex.raw('count(DISTINCT userid)'), knex.raw('array_agg(DISTINCT userid)')]).groupBy('docid')
    .then(function(rows) {
        var tab = _.filter(rows, function(row) { return row.count > 1; });
        var i, j;
        for(i=0; i<tab.length; ++i) {
            for(j=0; j<tab.length; ++j) {
                if (i!=j) {
                    var a = _.intersection(tab[i].array_agg, tab[j].array_agg);
                    if (a.length>0) {
                        var sim = a.length/tab[i].count;
                        add(tab[i], tab[j], sim);
                    } 
                }
            }
        }
        return _.map(tab, function(i) {
            return {
                "docid": i.docid,
                "count": Number(i.count),
                "sim": JSON.stringify(i.sim),
                "doc": JSON.stringify(i.doc)
            };
        });
    }).then(function(rows) {
        return knex.batchInsert('lrs_msim', rows, chunk_size).returning('id');
    }).then(function(ids) {
        console.log(ids);
    }).finally(function() {
        knex.destroy();
    });
