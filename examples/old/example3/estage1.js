// filter
var fs = require('fs');
var csv = require('fast-csv');

var users = {};  
/* {    userID1: [book1, book2, ...]
		userID2: [book1, book2, ...]
		.......: [.................]
		userIDN: [book1, book2, ...]
	}    
*/

function addBookToUser(record) {
	if (users.hasOwnProperty(record.userId)) {
		users[record.userId].push(record.docId);
	} else {
		users[record.userId] = [record.docId];
	}
}

function delBadUser() {
	for (user in users) {
		if (users[user].length==1 || users[user].length>=1000) {
			delete users[user]
			
		}
	}
}

var csvStream = fs.createReadStream('tmp/acirc2015.csv');

csv.fromStream(csvStream, {delimiter: '\t', headers: ["userId", "docId"]})
	.on('data', function(data) {
		addBookToUser(data);
	})
	.on('end', function(data) {
		delBadUser();
		console.log('Read finished!!');
		var csvStream2 = csv.createWriteStream({delimiter: '\t', headers: ["userId", "docId"], });
		csvStream2.pipe(fs.createWriteStream("tmp/_estage1.csv"));		
		for (user in users) {
			for (var book=0; book<users[user].length; book++) {
				 csvStream2.write([user,users[user][book]]);
			} 
		}
		csvStream2.end();
		console.log('finish!!');
	});
