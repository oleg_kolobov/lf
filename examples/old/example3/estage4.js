// database.js
var pg = require('pg');
var _ = require('underscore');
var dburl = process.env.DATABASE_URL || 'postgres://lfusr1:library@10.10.10.104/lfdb';

var client = new pg.Client(dburl);


function add(d1, d2, sim) {
    var k = 5;
    var i;
    if (!d1.sim) {
        d1.sim = [];
        d1.doc = [];
        for (i=0; i<k; ++i) {
            d1.sim[i] = d1.sim[i] || 0;
            d1.doc[i] = d1.doc[i] || 0;
        }
    }

    for (i=0; i<k; ++i) {
        if (d1.sim[i] < sim) {
            var keep_sim = d1.sim[i];
            var keep_doc = d1.doc[i];
            d1.sim[i] = sim;
            d1.doc[i] = d2.docid;
            for(j=i+1; j<k; ++j) {
                var keep2_sim = d1.sim[j];
                var keep2_doc = d1.doc[j];
                d1.sim[j] = keep_sim;
                d1.doc[j] = keep_doc;
                keep_sim = keep2_sim;
                keep_doc = keep2_doc;
            }
            break;
        }
    }
}



client.connect(function(err) {
    if (err) {
        console.error(err);
    }
    var tab = [];
    var query = client.query("SELECT docid, count(DISTINCT userid), array_agg(DISTINCT userid) FROM lrs_learn GROUP BY docid");
    query.on('row', function(row) {
		if (row.count > 1) {
            tab.push(row);
        }
    });
    query.on('end', function() {
        var i, j;
        for(i=0; i<tab.length; ++i) {
            for(j=0; j<tab.length; ++j) {
                if (i!=j) {
                    var a = _.intersection(tab[i].array_agg, tab[j].array_agg);
                    if (a.length>0) {
                        var sim = a.length/tab[i].count;
                        add(tab[i], tab[j], sim);
                       
                    } 
                }
            }
        }
        client.query('DROP TABLE IF EXISTS lrs_msim', function(err, result) {
            if (err) {
                console.error(err);
            }
            //console.log(result);
        });
 
        client.query('CREATE TABLE lrs_msim(docid varchar(255) not null, count integer, sim real[], doc varchar(255)[] )', function(err, result) {
            if (err) {
                console.error(err);
            }
        });
    
        _.each(tab, function(item) {
            client.query(
                "INSERT INTO lrs_msim(docid, count, sim, doc) values($1, $2, $3, $4)",
                [item.docid, item.count, item.sim, item.doc]
            );
        });
		
        var query2 = client.query("SELECT sim, doc FROM lrs_msim");
        var results = [];
        query2.on('row', function(row) {
            results.push(row);
        });
        query2.on('end', function() {
            client.end();
            console.log(results);
        });  
    });
});
