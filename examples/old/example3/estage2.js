var fs = require('fs');
var csv = require('fast-csv');

var users = {}; // {userID : [book1, book2, ...]}

function addBookToUser(record) {
	if (users.hasOwnProperty(record.userId)) {
		users[record.userId].push(record.docId);
	} else {
		users[record.userId] = [record.docId];
	}
}





var csvStream = fs.createReadStream('tmp/_estage1.csv');


csv.fromStream(csvStream, {delimiter: '\t', headers: ["userId", "docId"], strictColumnHandling: true})
	.on('data', function(data) {
		addBookToUser(data);	
	})
	.on('end', function(data) {
		console.log('Read finished!!');
// set of learn data
		var csvStream2 = csv.createWriteStream({headers: ["userId", "docId"], delimiter: '\t'});
		csvStream2.pipe(fs.createWriteStream("tmp/_estage2l.csv"));
		var hf = Math.round(Object.keys(users).length*0.85);
		var count = 0;
		for (user in users) {
			count=count+1;
			if (count <= hf) {
				for (var book=0; book < users[user].length; book++) {
					 csvStream2.write([user,users[user][book]]);
				}
			}
		}
		console.log('hf=',hf, 'count=',count)
		csvStream2.end();
// set of test data
		var csvStream3 = csv.createWriteStream({headers: ["userId", "docId"], delimiter: '\t'});
		csvStream3.pipe(fs.createWriteStream("tmp/_estage2t.csv"));
		var hf = Math.round(Object.keys(users).length*0.85);
		var count = 0;
		for (user in users) {
			count=count+1;
			if (count > hf) {
				for (var book=0; book < users[user].length; book++) {
					 csvStream3.write([user,users[user][book]]);
				}
			}
		}
		console.log('hf2=',hf, 'count2=',count)
		csvStream3.end();
	});	

