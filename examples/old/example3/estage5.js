var pg = require('pg');
var _ = require('underscore');
var dburl = process.env.DATABASE_URL || 'postgres://lfusr1:library@10.10.10.104/lfdb';

var client = new pg.Client(dburl);


function getRecomDocs(docid, sim, N) {
    N = N || 3;
    for (var i=0; i<sim.length; ++i){
        if (docid.trim() == (sim[i].docid).trim()) {
            var res = [];            
            if (sim[i].doc != undefined) {
                for(var j=0; j<N; ++j) {
                    res.push(sim[i].doc[j]);
                }
            }
            break;
        }
        
    }
    return res;
}

function isHidDoc(hid,recd) {
   // var a = _.intersection([hid], recd)
   // return a
  for (var key in recd) {
      recd[key] = recd[key].trim();
  }  
  return _.intersection([hid.trim()], recd).length>0 ? 1 : 0;         
} 
    

function getRecall(hid, test, sim, N) {
    // here code
    var count_1 = [];
    for (var i=0; i<test.length; ++i){
        if (hid.trim() != (test[i].docid).trim()) {     
            var r_i = getRecomDocs(test[i].docid, sim, N)
            count_1.push(isHidDoc(hid,r_i))
        }
    }
    var sum_1 = count_1.reduce(function(sum, current) {
                        return sum + current;          }, 0);
    return sum_1/count_1.length;
} 

client.connect(function(err) {
    if (err) {
        console.error(err);
    }
   
    var query2 = client.query("SELECT DISTINCT docid FROM lrs_test GROUP BY docid");
	var test = [];
	query2.on('row', function(row) {
		test.push(row);
//		console.log(row.sim);
	});
	query2.on('end', function() {      
		client.end();
	}); 
    
    var query = client.query("SELECT * FROM lrs_msim");
	var msim = [];
	query.on('row', function(row) {
		msim.push(row);
//		console.log(row.sim);
	});
	query.on('end', function() {
        var hid_elem = test[0].docid;
//        console.log(test)
        
        
        var str1 = ' 0013f5ab3f41894c2081cc961868384eaafcddf0';
        var strhid = ' c7829f0dc52d2af5b026b8e580e75d645f5325ac';
//        var recs = getRecomDocs(str1, msim)
//        var ish = isHidDoc(str2,recs)
//        console.log(ish, recs);
        
        
        var recallavr = [];
        for (var i=0; i<5; ++i){
            recallavr.push(getRecall(test[i].docid, test, msim, 3))
        }
        console.log(recallavr)   
        

        
/*        for(var i=1; i<msim.length; ++i) {
//            console.log(i);
            if (msim[i].doc != undefined){
//                 console.log(i, msim[i].doc)
                for(var j=0; j<3; ++j) {
                   // count++;
    //                console.log(msim[3].doc[4])
  //                  console.log(msim[i].doc)
                }
            }
        
        } */
//        console.log(msim)   
        console.log('Norm')

	});
});



/* {    userID1: [book1, book2, ...]
        userID2: [book1, book2, ...]
        .......: [.................]
        userIDN: [book1, book2, ...]
    }    
*/
