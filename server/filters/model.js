'use strict'

const bookshelf = require('../../bookshelf');

require('../matrices/model');

const Filter = bookshelf.Model.extend({
    tableName: 'filters',

    hasTimestamp: "true",

    relationships: [
        "matrices"
    ], 

    matrices() {
        return this.hasMany('Matrix', 'filter_id');
    }

});

module.exports = bookshelf.model('Filter', Filter);
