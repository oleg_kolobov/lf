const raml2html = require('raml2html');
const ramlConfig = raml2html.getConfigForTheme();
const path = require('path');
const ramlFile = path.join(__dirname, 'lf.raml');

module.exports = function(req, res) {
    raml2html.render(ramlFile, ramlConfig).then(function(results) {
        res.send(results);
    }, function(error) {
        res.status(500).send(error);
    });
};

