'use strict'

const bookshelf = require('../../bookshelf');

require('../matrices/model');

const Recommendation = bookshelf.Model.extend({
    tableName: 'recommendations',

    hasTimestamp: "true",

    matrix() {
        return this.belongsTo('Matrix');
    }

});

module.exports = bookshelf.model('Recommendation', Recommendation);
