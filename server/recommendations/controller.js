const bookshelf = require('../../bookshelf');
const Recommendation = require('./model');
const omitopts = {omitPivot: false};

const verbose = (process.env.VERBOSE === '1') ? true : false;

exports.create = function(req, res) {
    const data = req.body;
    bookshelf.transaction(function(t) {
        return Recommendation.forge(data)
            .save(null, {
                method: 'insert',
                transacting: t
            })
            .then(function(saved_model) {
                res.status(201).json({success: "true", data: saved_model.toJSON(omitopts)});
            }).catch(function(err) {
                res.status(500).json({success: "false", data: {error: err}});
            });
    });
};

exports.index = function(req, res) {
    const limit = req.query.limit || 20;
    const offset = req.query.offset;
    const matrix_id = req.query.matrix_id;
    const docid = req.query.docid;

    bookshelf.transaction(function(t) {
        return Recommendation.query(function(qb) {
            if (matrix_id) {
                qb.where('matrix_id', matrix_id);
                if (docid) qb.andWhere('docid', docid);
            } else if(docid) {
                if (docid) qb.where('docid', docid);
            }
            console.log(qb.toString());
        }).fetchPage({
            limit: limit,
            offset: offset,
            transacting: t
        }).then(function(results) {
            res.json({success: "true", data: results.toJSON(omitopts)});
        }).catch(function(err) {
            res.status(500).json({success: "false", data: {error: err}});
        });
    });
};

exports.fetch = function(req, res) {
    console.log('fetch');
    const id = req.params.id;
    bookshelf.transaction(function(t) {
        return Recommendation.forge({id: id}).fetch({
            transacting: t
        }).then(function(model) {
            res.json({success: "true", data: model.toJSON(omitopts)});
        }).catch(function(err) {
            res.status(500).json({success: "false", data: {error: err}});
        });
    });
};

exports.update = function(req, res) {
    console.log('update');
    const id = req.params.id;
    const data = req.body;
    bookshelf.transaction(function(t) {
        return Recommendation.forge({id: id}).save(data, {
            method: 'update',
            transacting: t
        }).then(function(updated_model) {
            res.json({success: "true", data: updated_model.toJSON(omitopts)});
        }).catch(function(err) {
            console.log(err);
            res.status(500).json({success: "false", data: {error: err}});
        });
    });
};

exports.del = function(req, res) {
    const id = req.params.id;
    bookshelf.transaction(function(t) {
        return Recommendation.forge({id: id}).destroy({transacting: t}).then(function(model) {
            res.json({success: "true", data: model.toJSON(omitopts)});
        }).catch(function(err) {
            res.status(500).json({success: "false", data: {error: err}});
        });
    });
};

exports.count = function(req, res) {
    console.log('count');
    Recommendation.count().then(function(results) {
        res.json({success: "true", data: results});
    }).catch(function(err) {
        res.status(500).json({success: "false", data: {error: err}});
    });
};
