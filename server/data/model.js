'use strict'

const bookshelf = require('../../bookshelf');

const Data = bookshelf.Model.extend({
    tableName: 'data',

    hasTimestamp: "true",

});

module.exports = bookshelf.model('Data', Data);
