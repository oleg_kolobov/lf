const bookshelf = require('../../bookshelf');
const Data = require('./model');
const omitopts = {omitPivot: false};

const verbose = (process.env.VERBOSE === '1') ? true : false;

exports.create = function(req, res) {
    const data = req.body;
    bookshelf.transaction(function(t) {
        return Data.forge(data)
            .save(null, {
                method: 'insert',
                transacting: t
            })
            .then(function(saved_model) {
                res.status(201).json({success: "true", data: saved_model.toJSON(omitopts)});
            }).catch(function(err) {
                res.status(500).json({success: "false", data: {error: err}});
            });
    });
};

exports.index = function(req, res) {
    const limit = req.query.limit;
    const offset = req.query.offset;
    bookshelf.transaction(function(t) {
        return Data.fetchPage({
            limit: limit,
            offset: offset,
            transacting: t
        }).then(function(results) {
            res.json({success: "true", data: results.toJSON(omitopts)});
        }).catch(function(err) {
            res.status(500).json({success: "false", data: {error: err}});
        });
    });
};

exports.fetch = function(req, res) {
    const id = req.params.id;
    bookshelf.transaction(function(t) {
        return Data.forge({id: id}).fetch({
            transacting: t
        }).then(function(model) {
            res.json({success: "true", data: model.toJSON(omitopts)});
        }).catch(function(err) {
            res.status(500).json({success: "false", data: {error: err}});
        });
    });
};

exports.update = function(req, res) {
    const id = req.params.id;
    const data = req.body;
    bookshelf.transaction(function(t) {
        return Data.forge({id: id}).save(data, {
            method: 'update',
            transacting: t
        }).then(function(updated_model) {
            res.json({success: "true", data: updated_model.toJSON(omitopts)});
        }).catch(function(err) {
            console.log(err);
            res.status(500).json({success: "false", data: {error: err}});
        });
    });
};

exports.del = function(req, res) {
    const id = req.params.id;
    bookshelf.transaction(function(t) {
        return Data.forge({id: id}).destroy({transacting: t}).then(function(model) {
            res.json({success: "true", data: model.toJSON(omitopts)});
        }).catch(function(err) {
            res.status(500).json({success: "false", data: {error: err}});
        });
    });
};

exports.count = function(req, res) {
    Data.count().then(function(results) {
        res.json({success: "true", data: results});
    }).catch(function(err) {
        res.status(500).json({success: "false", data: {error: err}});
    });
};
