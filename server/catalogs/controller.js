const bookshelf = require('../../bookshelf');
const Catalog = require('./model');
const omitopts = {omitPivot: false};

const verbose = (process.env.VERBOSE === '1') ? true : false;

const related_fields = [
];

exports.create = function(req, res) {
    const data = req.body;
    bookshelf.transaction(function(t) {
        return Catalog.forge(data)
            .save(null, {
                method: 'insert',
                transacting: t
            })
            .then(function(saved_model) {
                res.status(201).json({success: "true", data: saved_model.toJSON(omitopts)});
            }).catch(function(err) {
                res.status(500).json({success: "false", data: {error: err}});
            });
    });
};

exports.index = function(req, res) {
    const limit = req.query.limit;
    const offset = req.query.offset;
    bookshelf.transaction(function(t) {
        return Catalog.fetchPage({
            limit: limit,
            offset: offset,
            withRelated: related_fields,
            transacting: t
        }).then(function(results) {
            res.json({success: "true", data: results.toJSON(omitopts)});
        }).catch(function(err) {
            res.status(500).json({success: "false", data: {error: err}});
        });
    });
};

exports.fetch = function(req, res) {
    const id = req.params.id;
    bookshelf.transaction(function(t) {
        return Catalog.forge({id: id}).fetch({
            withRelated: related_fields,
            transacting: t
        }).then(function(model) {
            res.json({success: "true", data: model.toJSON(omitopts)});
        }).catch(function(err) {
            res.status(500).json({success: "false", data: {error: err}});
        });
    });
};

exports.update = function(req, res) {
    const id = req.params.id;
    const data = req.body;
    bookshelf.transaction(function(t) {
        return Catalog.forge({id: id}).save(data, {
            method: 'update',
            transacting: t
        }).then(function(updated_model) {
            res.json({success: "true", data: updated_model.toJSON(omitopts)});
        }).catch(function(err) {
            console.log(err);
            res.status(500).json({success: "false", data: {error: err}});
        });
    });
};

exports.del = function(req, res) {
    const id = req.params.id;
    bookshelf.transaction(function(t) {
        return Catalog.forge({id: id}).destroy({transacting: t}).then(function(model) {
            res.json({success: "true", data: model.toJSON(omitopts)});
        }).catch(function(err) {
            res.status(500).json({success: "false", data: {error: err}});
        });
    });
};

exports.count = function(req, res) {
    Catalog.count().then(function(results) {
        res.json({success: "true", data: results});
    }).catch(function(err) {
        res.status(500).json({success: "false", data: {error: err}});
    });
};

exports.search = function(req, res) {
    res.json({success: "false", data: []});
};
