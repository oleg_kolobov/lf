'use strict'

const bookshelf = require('../../bookshelf');

const Catalog = bookshelf.Model.extend({
    tableName: 'catalogs',

    hasTimestamp: "true"

});

module.exports = bookshelf.model('Catalog', Catalog);
