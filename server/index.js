/**
 * index.js -- server
 */
const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');

const app = express();

if (app.get('env') == 'development') {
    if (process.env.USE_MORGAN == '1') {
        app.use(morgan('combined'));
    }
}


app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

app.get("/", function(req, res) {
    res.json({
        "npm_package_name": process.env.npm_package_name,
        "npm_package_version": process.env.npm_package_version
    });
});

app.get('/docs', require('./docs'));

var router = express.Router();

//
// Data
//
router.post('/data', require('./data/controller').create);
router.get('/data', require('./data/controller').index);
router.get('/data/count', require('./data/controller').count);
router.get('/data/:id', require('./data/controller').fetch);
router.put('/data/:id', require('./data/controller').update);
router.delete('/data/:id', require('./data/controller').del);
//
// Filters 
//
router.post('/filters', require('./filters/controller').create);
router.get('/filters', require('./filters/controller').index);
router.get('/filters/count', require('./filters/controller').count);
router.get('/filters/:id', require('./filters/controller').fetch);
router.put('/filters/:id', require('./filters/controller').update);
router.delete('/filters/:id', require('./filters/controller').del);
//
// Matrices 
//
router.post('/matrices', require('./matrices/controller').create);
router.get('/matrices', require('./matrices/controller').index);
router.get('/matrices/count', require('./matrices/controller').count);
router.get('/matrices/:id', require('./matrices/controller').fetch);
router.put('/matrices/:id', require('./matrices/controller').update);
router.delete('/matrices/:id', require('./matrices/controller').del);
router.get('/matrices/:id/:command', require('./matrices/controller').command);
router.post('/matrices/:id/:command', require('./matrices/controller').command);

//
// Recommendations 
//
router.post('/recommendations', require('./recommendations/controller').create);
router.get('/recommendations', require('./recommendations/controller').index);
router.get('/recommendations/count', require('./recommendations/controller').count);
router.get('/recommendations/:id', require('./recommendations/controller').fetch);
router.put('/recommendations/:id', require('./recommendations/controller').update);
router.delete('/recommendations/:id', require('./recommendations/controller').del);

//
// Catalogs 
//
router.post('/catalogs', require('./catalogs/controller').create);
router.get('/catalogs', require('./catalogs/controller').index);
router.get('/catalogs/count', require('./catalogs/controller').count);
router.get('/catalogs/:id', require('./catalogs/controller').fetch);
router.put('/catalogs/:id', require('./catalogs/controller').update);
router.delete('/catalogs/:id', require('./catalogs/controller').del);
router.get('/catalogs/:id/:docid', require('./catalogs/controller').search);



router.get('*', function(req, res) {
    res.status(404).send({success: false, message: 'Not found'});
});

app.use('/api/v1', router);

module.exports = app;
