const _ = require('underscore');
const knex = require('../../knex/knex.js');

const chunk_size = 1000;

function add(d1, d2, sim, k) {
    var i;

    k = k || 5; // default value

    if (!d1.sim) {
        d1.sim = [];
        d1.doc = [];
        for (i=0; i<k; ++i) {
            d1.sim[i] = d1.sim[i] || 0;
            d1.doc[i] = d1.doc[i] || 0;
        }
    }

    for (i=0; i<k; ++i) {
        if (d1.sim[i] < sim) {
            var keep_sim = d1.sim[i];
            var keep_doc = d1.doc[i];
            d1.sim[i] = sim;
            d1.doc[i] = d2.docid;
            for(j=i+1; j<k; ++j) {
                var keep2_sim = d1.sim[j];
                var keep2_doc = d1.doc[j];
                d1.sim[j] = keep_sim;
                d1.doc[j] = keep_doc;
                keep_sim = keep2_sim;
                keep_doc = keep2_doc;
            }
            break;
        }
    }
}

exports.create = function(options, done) {
    options = options || {};

    if (options.dataset) {
        knex('data')
            .select(['docid', knex.raw('count(DISTINCT userid)'), knex.raw('array_agg(DISTINCT userid)')])
            .where('dataset', options.dataset)
            .groupBy('docid')
            .then(function(rows) {
                let tab = _.filter(rows, function(row) { return row.count > 1; });
                let i, j;
                for(i=0; i<tab.length; ++i) {
                    for(j=0; j<tab.length; ++j) {
                        if (i!=j) {
                            let a = _.intersection(tab[i].array_agg, tab[j].array_agg);
                            if (a.length>0) {
                                let sim = a.length/tab[i].count;
                                let k = JSON.parse(options.filter_parameters).k;
                                add(tab[i], tab[j], sim, k);
                            } 
                        }
                    }
                }
                return _.map(tab, function(i) {
                    return {
                        "matrix_id": options.matrix_id,
                        "docid": i.docid,
                        "count": Number(i.count),
                        "sim": JSON.stringify(i.sim),
                        "doc": JSON.stringify(i.doc)
                    };
                });
            }).then(function(rows) {
                return knex.batchInsert('recommendations', rows, chunk_size).returning('id');
            }).then(function(ids) {
                done(null, ids);
            }).finally(function() {
                //knex.destroy();
            });
    } else {
        done(new Error("No dataset"), null);
    }
};
exports.drop = function(options, done) {
    options = options || {};
    if (options.dataset) {
        knex('recommendations').where('matrix_id', options.matrix_id).del().then(function(results) {
            done(null, results);
        }).catch(function(err) {
            done(err.message || err, null);
        });
    } else {
        done(new Error("No dataset"), null);
    }
};
