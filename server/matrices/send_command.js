const gearman = require('../../gearman');

const lf = require('./lf');

gearman.registerWorker('myworker', function(payload, worker) {

    if (!payload) {
        worker.error();
        return;
    }

    let p = JSON.parse(payload.toString("UTF-8"));

    lf(p.command, p.args, function(err, data) {
        if (err) {
            console.log('worker error:', err);
        } else {
            console.log('worker data:', data);
        }
    });

    worker.end("OK");
});
gearman.registerWorker('myworker', function(payload, worker) {

    if (!payload) {
        worker.error();
        return;
    }

    let p = JSON.parse(payload.toString("UTF-8"));

    if (p.command === 'create') {
        lf.create(p.args, function(err, data) {
            if (err) {
                console.log('command create error:', err);
            } else {
                console.log('command create results:', data);
            }
        });
    } else if (p.command === "drop") {
        lf.drop(p.args, function(err, data) {
            if (err) {
                console.log('command drop error:', err);
            } else {
                console.log('command drop results:', data);
            }
        });
    } else {
        console.log('Bad command');
    }
    worker.end("OK");
});


function send_command(command, args, done) {

    args = args || {};

    if (command === 'create' || command === 'drop') {

        let payload = {
            command: command,
            args: args
        };
        let error = null;
        let data = '';

        gearman.submitJob('myworker', JSON.stringify(payload))
            .on("error", function(err) {
                error = err;
            })
            .on("data", function(chunk) {
                data += chunk;
            })
            .on("end", function() {
                done(error, data);
            });

    } else {

        done(new Error("Bad command"), null);

    }
};

module.exports = send_command;
