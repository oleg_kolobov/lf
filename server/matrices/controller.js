const bookshelf = require('../../bookshelf');
const Matrix = require('./model');
const omitopts = {omitPivot: false};
const send_command = require('./send_command');

const verbose = (process.env.VERBOSE === '1') ? true : false;

const related_fields = [
    'filter'
];

exports.create = function(req, res) {
    const data = req.body;
    bookshelf.transaction(function(t) {
        return Matrix.forge(data)
            .save(null, {
                method: 'insert',
                transacting: t
            })
            .then(function(saved_model) {
                res.status(201).json({success: "true", data: saved_model.toJSON(omitopts)});
            }).catch(function(err) {
                res.status(500).json({success: "false", data: {error: err}});
            });
    });
};

exports.index = function(req, res) {
    const limit = req.query.limit;
    const offset = req.query.offset;
    bookshelf.transaction(function(t) {
        return Matrix.fetchPage({
            limit: limit,
            offset: offset,
            withRelated: related_fields,
            transacting: t
        }).then(function(results) {
            res.json({success: "true", data: results.toJSON(omitopts)});
        }).catch(function(err) {
            res.status(500).json({success: "false", data: {error: err}});
        });
    });
};

exports.fetch = function(req, res) {
    const id = req.params.id;
    bookshelf.transaction(function(t) {
        return Matrix.forge({id: id}).fetch({
            withRelated: related_fields,
            transacting: t
        }).then(function(model) {
            res.json({success: "true", data: model.toJSON(omitopts)});
        }).catch(function(err) {
            res.status(500).json({success: "false", data: {error: err}});
        });
    });
};

exports.update = function(req, res) {
    const id = req.params.id;
    const data = req.body;
    bookshelf.transaction(function(t) {
        return Matrix.forge({id: id}).save(data, {
            method: 'update',
            transacting: t
        }).then(function(updated_model) {
            res.json({success: "true", data: updated_model.toJSON(omitopts)});
        }).catch(function(err) {
            console.log(err);
            res.status(500).json({success: "false", data: {error: err}});
        });
    });
};

exports.del = function(req, res) {
    const id = req.params.id;
    bookshelf.transaction(function(t) {
        return Matrix.forge({id: id}).destroy({transacting: t}).then(function(model) {
            res.json({success: "true", data: model.toJSON(omitopts)});
        }).catch(function(err) {
            res.status(500).json({success: "false", data: {error: err}});
        });
    });
};

exports.count = function(req, res) {
    Matrix.count().then(function(results) {
        res.json({success: "true", data: results});
    }).catch(function(err) {
        res.status(500).json({success: "false", data: {error: err}});
    });
};

exports.command = function(req, res) {
    const id = req.params.id;
    const command = req.params.command;
    bookshelf.transaction(function(t) {
        return Matrix.forge({id: id}).fetch({
            withRelated: related_fields,
            transacting: t
        }).then(function(model) {

            let modelp = model.toJSON();

            if(command === "create") {

                if (!model.get('complete')) {
                    let args = {};
                    args.matrix_id = model.id;
                    args.dataset = model.get('dataset');
                    args.filter_name = modelp.filter.name;
                    args.filter_parameters = modelp.filter.parameters;
                    send_command(command, args, function(err, results) {
                        if (err) {
                            res.status(500).json({success: "false", data: {error: err.message || err}});
                        } else {
                            model.set('complete', true).save().then(function() {
                                res.json({success: "true", data: results});
                            }).catch(function(err) {
                                res.status(500).json({success: "false", data: {error: err.message || err}});
                            });
                        }
                    });
                } else {
                    res.status(500).json({success: "false", data: {error: "Matrix already created"}});
                }


            } else if (command === "drop") {

                if (model.get('complete')) {
                    let args = {};
                    args.matrix_id = model.id;
                    args.dataset = model.get('dataset');
                    args.filter = model.get('filter');
                    send_command(command, args, function(err, results) {
                        if (err) {
                            res.status(500).json({success: "false", data: {error: err.message || err}});
                        } else {
                            model.set('complete', false).save().then(function() {
                                res.json({success: "true", data: results});
                            }).catch(function(err) {
                                res.status(500).json({success: "false", data: {error: err.message || err}});
                            });
                        }
                    });
                } else {
                    res.status(500).json({success: "false", data: {error: "Matrix not created"}});
                }

            } else {

                res.status(500).json({success: "false", data: {error: "Bad command"}});

            }

        }).catch(function(err) {
            res.status(500).json({success: "false", data: {error: err.message || err}});
        });
    });
};
