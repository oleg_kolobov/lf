'use strict'

const bookshelf = require('../../bookshelf');

require('../filters/model');
require('../recommendations/model');

const Matrix = bookshelf.Model.extend({
    tableName: 'matrices',

    hasTimestamp: "true",

    relationships: [
        "recommendations"
    ],

    filter() {
        return this.belongsTo('Filter');
    },

    recommendations() {
        return this.hasMany('Recommendation', 'matrix_id');
    }

});

module.exports = bookshelf.model('Matrix', Matrix);
